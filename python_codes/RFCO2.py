#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 10:44:30 2020

@author: herve.guillard@inria.fr
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

"""
   script solving a simple 0d one layer climate model 
   with CO2 dependent radiative forcing 
   The script can be used to define 
   emission scenarios limiting the mean temperature increase. 

"""
def scenario1(t):
  """
     Buissness as usual : no control of CO2 emission
     and constant emission rate of 2ppm/year
  """
  nsec_in_year=(365.25*24*60*60)
  CO2_rate=2./nsec_in_year
  
  return CO2_rate

def scenario2(t):
  """
     decrease by 45% of emission rate in 2030
     0 emission in 2050
  """
  sec_per_year=(365.25*24*60*60) 
  t2020=60*sec_per_year
  t2030=70*sec_per_year
  t2050=90*sec_per_year
  ppm_rate=2/sec_per_year # 2ppm per year 
  t_i=(t2030-t2020)/(t2050-t2020)
  decrease_i=0.45  # 0.45*ppm_rate : emission rate in 2030 
  p=np.empty(4)
  
  p[0]=ppm_rate
  p[3]=0
  p[2]=0
  p[1]=p[0]*(decrease_i-(1-t_i)**3)/(3*t_i*(1-t_i)**2)
  if t< t2020:
    CO2_rate=ppm_rate
  if t>= t2020 and t<t2050:
      s=(t-t2020)/(t2050-t2020)
      CO2_rate=p[0]*(1-s)**3 + (3.*p[1]*s*(1-s)**2) + (3*p[2]*(1-s)*s**2 )+ p[3]*s**3
  if t>= t2050: 
      CO2_rate=0      
    
  return CO2_rate

def RF(CO2):
  """
  compute the radiative forcng w.r.t CO2 concentration
  5.35 ln(C/C0) is the standard value of radiative forcing due to CO2
  """  
  CO20 =280 #preindustrial value 
  rf=5.35*np.log(CO2/CO20)
  return rf
   
def ebm(y, t,emission):
  """ simple one layer 0d energy budget model CO2 dependent emissivity"""
  """ geophysical constants """
  Cs=2.08e+8       # effective heat capacity of earth J K-1 m-2
  Ca=1.0e+7        # effective heat capacity of atmosphere
  ECS = 1.3        # Equilibrium climate sensitivity W.m-2.K-1
  if emission ==1:
    """ scenario 1 : 2ppm/year """
    CO2_rate=scenario1(t)
  if emission ==2:  
    """ scenario 2 : max in 2040, then stabilization of CO2 concentration """
    CO2_rate=scenario2(t)
  #if emission ==3:
  #""" input here your own emission scenario """
  
  temps, CO2 = y
  dydt = [(RF(CO2)-ECS*temps)/Cs,CO2_rate]
  return dydt

""" ------------------------------"""
""" define initial conditions """
""" ------------------------------"""
y0=[0.,310]# initial temperature in Kelvin and CO2 concentration in 1960

""" ------------------------------"""
""" define time range """
""" ------------------------------"""
tM_year=140.                         #Max time in years 
tmax=tM_year*365.25*24*60*60         #Max time in seconds

t_year = np.linspace(0, tM_year, 101) 
t_sec = np.linspace(0, tmax, 101)
""" ------------------------------"""
""" solve the ode """
""" ------------------------------"""
""" choose the emission scenario here """
emission=(1,) # scenario 1 : 2ppm/year """
#emission=(2,) #scenario 2 : 0 emission in 2050 

sol = odeint(ebm, y0, t_sec,args=emission)
Tsur=sol[:,0]
CO2=sol[:,1]

print('-----------------------------------')
if emission[0]==1:
  print('scenario 1 : constant CO2 emission of 2ppm/year')
  print('-----------------------------------')
if emission[0]==2:
  print('scenario 2 : emission decreases by 2020 with 0 emission in 2050')
  print('-----------------------------------')   
print('final temperature anomaly    :', sol[100,0],'celsius')
print('final CO2 concentration      :', sol[100,1],'ppm')
""" ------------------------------"""
""" plot the results """
""" ------------------------------"""
t_year=t_year+1960*np.ones_like(t_year)

fig, axs = plt.subplots(3, 1,figsize=(8,8))
axs[0].set_title('Example of hand-made RCP')

""" plot surface temperature """
color = 'tab:blue'
axs[0].set_xlabel('years')
axs[0].plot(t_year,Tsur, color=color,label='surface temperature anomaly')
axs[0].legend()
axs[0].grid(True)

""" plot radiative forcing """
color = 'tab:red'
rf= RF(CO2)
axs[1].set_ylabel('radiative forcing W/m-2', color=color)
axs[1].plot(t_year,rf, color=color)
axs[1].grid(True)

""" plot CO2 emission """
color = 'tab:green'
axs[2].set_ylabel('CO2 emission in ppm/year', color=color)
y=np.zeros_like(t_sec)
if emission[0]==1:
  for i in range(len(t_sec)):
    y[i]=(365.25*24*60*60)*scenario1(t_sec[i])
if emission[0]==2:
  for i in range(len(t_sec)):
    y[i]=(365.25*24*60*60)*scenario2(t_sec[i])   
axs[2].plot(t_year, y, color=color)

plt.show()