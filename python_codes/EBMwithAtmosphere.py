#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 10:44:30 2020

@author: herve.guillard@inria.fr
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

def ebm(y, t):
  """ simple two layers 0d energy budget model """
  """ geophysical constants """
  Cs=2.08e+8       # effective heat capacity of earth J K-1 m-2
  Ca=1.0e+7        # effective heat capacity of atmosphere
  S=1370           # solar constant in w m-2
  alpha =0.3       # albedo
  sigma = 5.67e-8  # Boltzmann constant w m-2 k-4 
  epsilon = 0.77   # atmosphere gray body emmissivity 
  temps, tempa = y

  dydt = [((1-alpha)*S/4. - sigma*temps**4 + epsilon*sigma*tempa**4)/Cs,\
          (sigma*epsilon*temps**4.-2*epsilon*sigma*tempa**4)/Ca ]
  return dydt
""" ------------------------------"""
""" define initial conditions """
""" ------------------------------"""
y0=[200,400]           # initial temperature in Kelvin 
""" ------------------------------"""
""" define time range """
""" ------------------------------"""
tM_year=30.                         #Max time in years 
tmax=tM_year*365.25*24*60*60        # Max time in seconds

t_year = np.linspace(0, tM_year, 101) 
t_sec = np.linspace(0, tmax, 101)
""" ------------------------------"""
""" solve the ode """
""" ------------------------------"""
sol = odeint(ebm, y0, t_sec)
solCelsius=sol-273*np.ones_like(sol) # temperature in Celsius 
print('-----------------------------------')
print('final surface temperature    :', solCelsius[100,0],'celsius')
print('final atmosphere temperature :', solCelsius[100,1],'celsius' )
""" ------------------------------"""
""" plot the results """
""" ------------------------------"""
plt.plot(t_year, solCelsius[:,0], label='surface temperature',color='blue',lw=2)
plt.plot(t_year, solCelsius[:,1], label='atmosphere temperature',color='red',lw=2)
plt.legend(loc='best')
plt.xlabel('years')
plt.grid()
plt.show()