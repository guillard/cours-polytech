#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 10:44:30 2020

@author: herve.guillard@inria.fr
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

def ebm(y, t):
  """simple two layers 0d energy budget model with fixed surface temperature """
  """ geophysical constants """
  Cs=2.08e+8       # effective heat capacity of earth J K-1 m-2
  Ca=1.0e+7        # effective heat capacity of atmosphere
  S=1370           # solar constant in w m-2
  alpha =0.3       # albedo
  sigma = 5.67e-8  # Boltzmann constant w m-2 k-4 
  epsilon = 0.77  # atmosphere gray body emmissivity 
  temps, tempa = y

  dydt = [0,\
          (sigma*epsilon*temps**4.-2*epsilon*sigma*tempa**4)/Ca ]
  return dydt
""" ------------------------------"""
""" define initial conditions """
""" ------------------------------"""
T_sur=300
y0=[T_sur,400]           # initial temperature in Kelvin 
""" ------------------------------"""
""" define time range """
""" ------------------------------"""
tM_year=5.                         #Max time in years 
tmax=tM_year*365.25*24*60*60        # Max time in seconds

t_year = np.linspace(0, tM_year, 101) 
t_sec = np.linspace(0, tmax, 101)
""" ------------------------------"""
""" solve the ode """
""" ------------------------------"""
sol = odeint(ebm, y0, t_sec)
print('-----------------------------------')
print('Equilibrium air temperature  :', sol[100,0]/(2**0.25),'kelvin')
print('final atmosphere temperature :', sol[100,1],'kelvin' )
""" ------------------------------"""
""" plot the results """
""" ------------------------------"""
plt.plot(t_year, sol[:,0]/(2**0.25), label='Equilibrium air temperature',color='blue',lw=2)
plt.plot(t_year, sol[:,1], label='atmosphere temperature',color='red',linestyle='dashed',lw=2)
plt.legend(loc='best')
plt.xlabel('years')
plt.grid()
plt.show()