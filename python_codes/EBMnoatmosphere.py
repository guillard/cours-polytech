#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 10:44:30 2020

@author: herve.guillard@inria.fr
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
"""
   simple energy budget model of a planet with no atmosphere 
   
"""
def ebm(y, t):
  """ simple 0d energy budget model """
  """ geophysical constants """
  C=2.08e+8        # effective heat capacity of earth J K-1 m-2
  S=1365           # solar constant in w m-2
  alpha =0.32      # albedo
  sigma = 5.67e-8  # Stefan constant w m-2 k-4 
  epsilon = 1.     # gray body emmissivity 
  dydt = ((1-alpha)*S/4.-epsilon*sigma*y**4)/C
  return dydt
""" ------------------------------"""
""" define initial conditions """
""" ------------------------------"""
y0=300                             # initial temperature in Kelvin 
""" ------------------------------"""
""" define time range """
""" ------------------------------"""
tM_year=100.                        #Max time in years 

tmax=tM_year*365.25*24*60*60        #Max time in seconds

t_sec = np.linspace(0, tmax, 101)
""" ------------------------------"""
""" solve the ode """
""" ------------------------------"""
sol = odeint(ebm, y0, t_sec)        #array containing the solutions at each time 
solCelsius=sol-273*np.ones_like(sol)# temperature in Celsius 
""" ------------------------------"""
""" display the results"""
""" ------------------------------"""
t_year = np.linspace(0, tM_year, 101)
print('--------------------------------------')
print('final temperature after',tM_year, 'years : ', solCelsius[100] )
print('--------------------------------------')
""" ------------------------------"""
""" plot the results """
""" ------------------------------"""
plt.plot(t_year, solCelsius[:], 'b', label='temperature',lw=2)
plt.legend(loc='best')
plt.xlabel('years')
plt.grid()
plt.show()